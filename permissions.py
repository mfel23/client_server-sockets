from enum import StrEnum

class Permissions(StrEnum):
    ADMIN = "Administrator"
    USER = "User"

    @classmethod
    def has_value(cls, value):
        return value in cls._value2member_map_ 
    