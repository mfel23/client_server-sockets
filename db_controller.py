import sqlite3
from sqlite3 import Error
from datetime import datetime


class Db_SQLite:

    def __init__(self, dbname):
        self.db_name = dbname
        self.connection = self._create_connection()
        self.cursor = self._create_cursor()

    def _create_connection(self):
        conn = None
        try:
            conn = sqlite3.connect(self.db_name)
        except Error as e:
            print(e)
        return conn

    def _create_cursor(self):
        try:
            c = self.connection.cursor()
        except Error as e:
            print(e)
        finally:
            return c

    def setup(self):
        cursor = self.connection.cursor()
        cursor.execute("""
            DROP TABLE users
            """)
        cursor.execute("""
            DROP TABLE messages
            """)
        cursor.execute(""" CREATE TABLE IF NOT EXISTS users (
                                    user_id INTEGER PRIMARY KEY NOT NULL,
                                    user_password varchar(40) NOT NULL,
                                    user_name varchar(40) NOT NULL,
                                    user_type varchar(20) NOT NULL,
                                    UNIQUE(user_name)
                                )""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS messages (
                                    message_id INTEGER PRIMARY KEY,
                                    sender_id INTEGER NOT NULL,
                                    receiver_id INTEGER NOT NULL,
                                    message varchar(255) NOT NULL,
                                    time timestamp NOT NULL,
                                    FOREIGN KEY (receiver_id) REFERENCES users (user_id) ON DELETE CASCADE
                                )""")
        cursor.execute(
            "INSERT INTO users (user_password, user_name, user_type) VALUES (?, ?, ?)",
            ("admin", "admin", "Administrator"))
        cursor.execute(
            "INSERT INTO messages (sender_id, receiver_id, message, time) VALUES (?, ?, ?, ?)",
            (1, 1, "haha", datetime.now().strftime('%d-%m-%Y %H-%M-%S')))
        self.connection.commit()
        # self.connection.close()

    def clear_db(self):
        with self.cursor as cur:
            cur.execute("""
                DROP TABLE IF EXISTS users, messages
                """)
        self.connection.commit()
        self.connection.close()
        return "Database cleared"


class SQLite_user_db:

    def __init__(self, database):
        self.database = database

    def create_user(self, username, password, usertype):

        self.database.cursor.execute(f"INSERT INTO users (user_password, user_name, user_type) VALUES (?, ?, ?)",
                                     (password, username, usertype))
        self.database.cursor.execute(
            f"SELECT user_id, user_name, user_password, user_type FROM users WHERE user_name = '{username}'")
        user = self.database.cursor.fetchone()
        self.database.connection.commit()
        print(user)
        return user

    def change_password(self, username, password):
        self.database.cursor.execute(
            f"UPDATE users SET user_password = '{password}' WHERE user_name = '{username}'")
        self.database.cursor.execute(
            f"SELECT user_id, user_name, user_password, user_type FROM users WHERE user_name = '{username}'")
        user = self.database.cursor.fetchone()
        self.database.connection.commit()
        return user

    def get_user_info(self, username):
        self.database.cursor.execute(
            f"SELECT user_id, user_name, user_password, user_type FROM users WHERE user_name = '{username}'")
        user_info = self.database.cursor.fetchone()
        self.database.connection.commit()
        return user_info

    def delete_user(self, username):
        self.database.cursor.execute(
            f"DELETE FROM users WHERE user_name = '{username}'")
        self.database.connection.commit()


class SQLite_message_db:

    def __init__(self, database) -> None:
        self.database = database

    def create_message(self, sender_id, receiver_id, message):
        self.database.cursor.execute(
            f"SELECT * FROM messages WHERE receiver_id = {receiver_id}")
        mailbox = self.database.cursor.fetchall()
        self.database.connection.commit()
        messages_quantity = len(mailbox)
        if messages_quantity >= 5:
            return False, "Mailbox is full"
        self.database.cursor.execute("INSERT INTO messages (sender_id, receiver_id, message, time) VALUES (?, ?, ?, ?)", (
            sender_id, receiver_id, message, datetime.now().strftime('%d-%m-%Y %H-%M-%S')))
        self.database.cursor.execute(
            f"SELECT sender_id, receiver_id, message FROM messages WHERE sender_id = '{sender_id}' ORDER BY time DESC LIMIT 1")
        latest_senders_message = self.database.cursor.fetchone()
        self.database.connection.commit()
        if latest_senders_message[0] == sender_id and latest_senders_message[1] == receiver_id and latest_senders_message[2] == message:
            return True, "OK"

    def delete_message(self, message, receiver_id, timee):
        self.database.cursor.execute(
            f"DELETE FROM messages WHERE message = '{message}' AND receiver_id = '{receiver_id}' AND time = '{timee}'")
        self.database.cursor.execute(
            f"SELECT message FROM messages WHERE message = '{message}' AND receiver_id = '{receiver_id}' AND time = '{timee}'")
        message_db = self.database.cursor.fetchone()
        self.database.connection.commit()
        if message_db == None:
            return True
        else:
            return False

    def read_oldest_message(self, readerid):
        self.database.cursor.execute(
            f"SELECT message, sender_id, receiver_id, time FROM messages WHERE receiver_id = '{readerid}' ORDER BY time ASC LIMIT 1")
        result = self.database.cursor.fetchone()
        self.database.connection.commit()
        if result == None:
            return False, "Empty mailbox", ""
        message_deleted = self.delete_message(
            message=result[0], receiver_id=result[1], timee=result[3])
        self.database.cursor.execute(
            f"SELECT user_name FROM users WHERE user_id = {result[1]}")
        sender_name = self.database.cursor.fetchone()[0]
        self.database.connection.commit()
        if message_deleted:
            return True, result[0], sender_name
