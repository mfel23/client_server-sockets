import socket
import json

class Client:

    def __init__(self, host, port) -> None:
        self.HOST = host  # The server's hostname or IP address
        self.PORT = port  # The port used by the server
        self.BUFFER = 1024
        self.status = True
        self.token = ""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.HOST, self.PORT))

    def type_command(self):
        command = input("Podaj komendę (help- lista komend):")
        return command
    
    def logout(self):
        command = f"logout {self.token}"
        return command
    
    def signin(self):
        username = input("user: ")
        password = input("password: ")
        command = f"signin {username} {password}"
        return command
    
    def adduser(self):
        username = input("user: ")
        password = input("password: ")
        usertype = input("define a role (Administrator / Default): ")
        command = f"adduser {username} {password} {usertype} {self.token}"
        return command

    def deluser(self):
        username = input("user: ")
        command = f"deluser {username} {self.token}"
        return command    
    
    def send_message(self):
        username = input("to user: ")
        message = input("message: ")
        command = ""
        if len(message) > 255:
            command = "messagetoolong"
        else:
            command = f"sendmessage {username} {self.token} {message}"
        return command
    
    def read_message(self):
        command = f"readmessage {self.token}"
        return command
    
    def change_password(self):
        password_old = input("Type your current password: ")
        password_new = input("Type your new password: ")
        command = f"changepassword {password_old} {password_new} {self.token}"
        return command

    def match_command(self, command):
        match command:
            case "logout":
                command = self.logout()
            case "signin":
                command = self.signin()
            case "adduser":
                command = self.adduser()
            case "deluser":
                command = self.deluser()
            case "sendmessage":
                command = self.send_message()
            case "readmessage":
                command = self.read_message()
            case "changepassword":
                command = self.change_password()
        return command
    
    def send_request(self, command):
        command_dict = {"Command": command}
        self.socket.send(json.dumps(command_dict).encode("utf8"))

    def receive_response(self):
        response = self.socket.recv(self.BUFFER)
        decoded_response = json.loads(response)
        return decoded_response
    
    def process_response(self, response):
        if type(response["response"]) is dict:
            for key, value in response["response"].items():
                if key == "token":
                    self.token = value
                else:
                    print(key, ":", value)
        else:
            if response["response"] == "stop":
                print("Client will be stopped")
                self.status = False
            elif response["response"] == "method unknown":
                print("You provided unknown method. Please type 'help' to list all available methods.")
            else:
                print(response["response"])

    def run_client(self):
        while self.status:
            command = self.type_command()
            command = self.match_command(command)
            self.send_request(command)
            response = self.receive_response()
            self.process_response(response)

client = Client("127.0.0.1", 65432)
client.run_client()