import socket
from datetime import datetime
import json
from dotenv import load_dotenv
from usersManager import UsersManager, InvalidTokenError
from messagesManager import MessagesManager


class Server:

    def __init__(self) -> None:
        self.HOST = "127.0.0.1"
        self.PORT = 65432
        self.BUFFER = 1024
        self.SERVER_VERSION = "1.0.0"
        self.server_start = datetime.now()
        self.status = True
        self.users_manager = UsersManager()
        self.messages_manager = MessagesManager()
        # self.db = DbController()

    def encode_dict(self, dict_arg):
        return json.dumps(dict_arg).encode("utf8")

    def info(self):
        info_dict = {
            "server version": self.SERVER_VERSION,
            "server created": f"{self.server_start}"
        }
        response = self.encode_dict({"response": info_dict})
        return response

    def uptime(self):
        now = datetime.now()
        duration = now - self.server_start
        duration_dict = {
            "uptime": f"{duration}"
        }
        response = self.encode_dict({"response": duration_dict})
        return response

    def help(self):
        commands_dict = {
            "uptime": "returns the duration of the server since it's started",
            "help": "returns all available commands you can use",
            "info": "returns the server version number and the date the server was created",
            "stop": "stops both client and server",
            "signin": "lets you sign-in to the system",
            "logout": "log-out from the system",
            "adduser": "add user if you've permissios for it",
            "deluser": "delete user",
            "changepassword": "change your password",
            "sendmessage": "send some message to other user",
            "readmessage": "read your oldest message and delete it"
        }
        response = self.encode_dict({"response": commands_dict})
        return response

    def stop(self):
        response = self.encode_dict({"response": "stop"})
        return response

    def signin(self, login, password):
        result, message = self.users_manager.sign_in(login, password)
        if not result:
            response = self.encode_dict({"response": f"{message}"})
            return response
        token = {"token": message}
        response = self.encode_dict({"response": token})
        return response

    def log_out(self, token):
        try:
            self.users_manager.log_out(token)
        except InvalidTokenError as e:
            error_response = {"error": str(e.message)}
            response = self.encode_dict({"response": error_response})
            return response
        else:
            response = self.encode_dict(
                {"response": "logged-out successfully"})
            return response

    def add_user(self, username, password, usertype, token):
        try:
            # result, message = self.users_manager.is_admin(token)
            # if not result:
            #     response = self.encode_dict({"response": f"{message}"})
            #     return response
            result, message = self.users_manager.add_user(
                username, password, usertype, token)
            if not result:
                response = self.encode_dict({"response": f"{message}"})
                return response
            response = self.encode_dict(
                {"response": f"user [{username}] created successfully"})
            return response
        except InvalidTokenError as e:
            error_response = {"error": str(e.message)}
            response = self.encode_dict({"response": error_response})
            return response

    def delete_user(self, username, token):
        try:
            result, message = self.users_manager.delete_user(username, token)
            if not result:
                response = self.encode_dict({"response": f"{message}"})
                return response
            response = self.encode_dict(
                {"response": f"user [{username}] deleted successfully"})
            return response
        except InvalidTokenError as e:
            error_response = {"error": str(e.message)}
            response = self.encode_dict({"response": error_response})
            return response

    def change_password(self, password_old, password_new, token):
        try:
            self.users_manager.change_password(
                password_old, password_new, token)
        except ValueError as e:
            error_response = {"error": e.args[0]}
            response = self.encode_dict({"response": error_response})
            return response
        except InvalidTokenError as e:
            error_response = {"error": str(e.message)}
            response = self.encode_dict({"response": error_response})
            return response
        else:
            response = self.encode_dict(
                {"response": f"Your password has been changed."})
            return response

    def send_message(self, receiver, token, message):
        try:
            if not self.users_manager.is_signed_in(token):
                return self.encode_dict({"error:": "You have to sign in to read a message"})
            sender_id = self.users_manager.get_user_id_by_token(token=token)
            result, info = self.messages_manager.send_message(
                sender_id, receiver, ' '.join(message))
        except InvalidTokenError as e:
            error_response = {"error": str(e.message)}
            response = self.encode_dict({"response": error_response})
            return response
        except MailboxOverloadError as e:
            error_response = {"error": str(e.message)}
            response = self.encode_dict({"response": error_response})
            return response
        else:
            if result == True:
                response = self.encode_dict({"response": f"message delivered"})
                return response
            else:
                response = self.encode_dict({"response": f"{info}"})

    def read_oldest_message(self, token):
        if not self.users_manager.is_signed_in(token):
            return self.encode_dict({"error:": "You have to sign in to read a message"})
        user_id = self.users_manager.get_user_id_by_token(token)
        result = self.messages_manager.read_oldest_message(user_id)
        if result[0] == "error":
            response = self.encode_dict({"response": f"error: {result[1]}"})
        else:
            response = self.encode_dict(
                {"response": f"message from {result[1]}: {result[0]}"})
        return response

    def unknown_command(self):
        response = self.encode_dict({"response": "method unknown"})
        return response

    def run_server(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.HOST, self.PORT))
        self.server_socket.listen()

        client_socket, address = self.server_socket.accept()

        while self.status:
            request = client_socket.recv(self.BUFFER)
            decoded_request_command = json.loads(request)["Command"].split(" ")
            print(decoded_request_command)

            match decoded_request_command[0]:
                case "uptime":
                    client_socket.send(self.uptime())
                case "info":
                    client_socket.send(self.info())
                case "help":
                    client_socket.send(self.help())
                case "stop":
                    self.status = False
                    client_socket.send(self.stop())
                    client_socket.close()
                    self.server_socket.close()
                    return
                case "signin":
                    login = decoded_request_command[1]
                    password = decoded_request_command[2]
                    client_socket.send(self.signin(login, password))
                case "logout":
                    token = decoded_request_command[1]
                    client_socket.send(self.log_out(token))
                case "adduser":
                    username = decoded_request_command[1]
                    password = decoded_request_command[2]
                    usertype = decoded_request_command[3]
                    token = decoded_request_command[4]
                    client_socket.send(self.add_user(
                        username, password, usertype, token))
                case "deluser":
                    username = decoded_request_command[1]
                    token = decoded_request_command[2]
                    client_socket.send(self.delete_user(username, token))
                case "changepassword":
                    password_old = decoded_request_command[1]
                    password_new = decoded_request_command[2]
                    token = decoded_request_command[3]
                    client_socket.send(self.change_password(
                        password_old, password_new, token))
                case "sendmessage":
                    receiver = decoded_request_command[1]
                    token = decoded_request_command[2]
                    message = decoded_request_command[3:]
                    client_socket.send(self.send_message(
                        receiver, token, message))
                case "readmessage":
                    token = decoded_request_command[1]
                    client_socket.send(self.read_oldest_message(token))
                case "messagetoolong":
                    response = self.encode_dict(
                        {"response": "Sorry, your massage is too long. 255 characters are available per one message"})
                    client_socket.send(response)
                case _:
                    client_socket.send(self.unknown_command())


server = Server()
server.run_server()
