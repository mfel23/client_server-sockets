
from permissions import Permissions
import secrets
from db_controller import Db_SQLite, SQLite_user_db
from dotenv import load_dotenv
import os


class InvalidTokenError(Exception):

    def __init__(self, *args: object) -> None:
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self) -> str:
        if self.message:
            return f"InvalidTokenError: {self.message}"
        else:
            return "InvalidTokenError has been raised"


class UsersManager:

    def __init__(self) -> None:
        load_dotenv()
        dbname = os.getenv("DB_NAME")
        self.tokens = {}
        database = Db_SQLite(dbname)
        database.setup()
        self.users_manager = SQLite_user_db(database=database)

    def delete_user(self, username, token):
        result, message = self.is_admin(token)
        if not result:
            return False, message
        self.users_manager.delete_user(username=username)
        return True, f"User {username} has been deleted"

    def change_password(self, password_old, password_new, token):
        username = self.get_username_by_token(token)
        user_info = self.users_manager.get_user_info(username=username)
        if user_info is not None:
            if user_info[1] == username and user_info[2] == password_old:
                user = self.users_manager.change_password(
                    username, password_new)
            else:
                return False, f"Wrong credentials!"
        return True, "Password has been changed!"

    def add_user(self, username, password, usertype, token):
        result, message = self.is_admin(token)
        if not result:
            return False, message
        if not Permissions.has_value(usertype):
            return False, "Usertype you provided is forbidden. Try to type correct usertype."
        new_user = self.users_manager.create_user(
            username=username, password=password, usertype=usertype)
        if new_user[1] == username and new_user[2] == password and new_user[3] == usertype:
            return True, "ok"
        else:
            return False, "Something went wrong"

    def get_username_by_token(self, token):
        tokens = self.tokens.values()
        if token in tokens:
            for key, value in self.tokens.items():
                if value == token:
                    username = key
                    return username
        else:
            raise InvalidTokenError("You're not signed-in")

    def get_user_id_by_token(self, token):
        tokens = self.tokens.values()
        if token in tokens:
            for key, value in self.tokens.items():
                if value == token:
                    username = key
                    user_id = self.users_manager.get_user_info(username)[0]
                    return user_id
        else:
            raise InvalidTokenError("You're not signed-in")

    def sign_in(self, username, password):
        user_info = self.users_manager.get_user_info(username=username)
        if user_info is not None:
            if user_info[1] == username and user_info[2] == password:
                token = f"{secrets.token_hex(14)}"
                self.tokens[f"{username}"] = token
                return True, token
            else:
                return False, "Wrong password"
        else:
            return False, "User doesn't exist"

    def log_out(self, token):
        tokens = self.tokens.values()
        if token in tokens:
            for key, value in self.tokens.items():
                if value == token:
                    del self.tokens[f"{key}"]
                    return True, "Logged-out successfully"
        else:
            raise InvalidTokenError("You're not signed-in")

    def is_signed_in(self, token):
        tokens = self.tokens.values()
        if token in tokens:
            return True
        else:
            return False

    def is_admin(self, token):
        if not self.is_signed_in(token):
            return False, "You are not signed-in"
        logged_user = self.get_username_by_token(token)
        logged_user_info = self.users_manager.get_user_info(logged_user)
        logged_user_type = logged_user_info[3]
        if not logged_user_type == "Administrator":
            return False, "You don't have administrator permissions"
        return True, "ok"
