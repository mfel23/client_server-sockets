
from db_controller import Db_SQLite, SQLite_message_db, SQLite_user_db
from dotenv import load_dotenv
import os


class MessagesManager:

    def __init__(self) -> None:
        load_dotenv()
        dbname = os.getenv("DB_NAME")
        database = Db_SQLite(dbname)
        self.messages_manager = SQLite_message_db(database=database)
        self.users_manager = SQLite_user_db(database=database)

    def read_oldest_message(self, readerid):
        result, message, sender_name = self.messages_manager.read_oldest_message(
            readerid)
        if result == False:
            return "error", message
        return message, sender_name

    def send_message(self, sender_id, receiver_name, message):
        receiver_id = self.users_manager.get_user_info(receiver_name)[0]
        result, message = self.messages_manager.create_message(
            sender_id=sender_id, receiver_id=receiver_id, message=message)
        return result, message
